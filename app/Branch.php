<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Branch extends Model
{
    protected $fillable = ['expiry_date','name','description','user_id','store_package_id','pay_month','pay_image_url','pay_status','color_1','color_2','img_logo_url'];
    protected $appends = ['price_cal','expiry_date_text'];
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function package()
    {
        return $this->belongsTo('App\StorePackage','store_package_id','id');
    }

    public function getExpiryDateTextAttribute(){
        return $this->expiry_date ? Carbon::parse($this->expiry_date)->format('d/m/Y'):null;
    }
    public function getPriceCalAttribute(){
        $result = 0;
        $cal = ($this->pay_month * $this->package->price);

        $result = $cal;

        $discount_sum = 0;

        if($this->pay_month >= 3){
            $result =  $cal - ($this->package->discount_3m/100) * $cal;
            $discount_sum =  ($this->package->discount_3m/100) * $cal;
        }else if($this->pay_month >= 6){
            $result =  $cal - ($this->package->discount_6m/100) * $cal;
            $discount_sum =  ($this->package->discount_6m/100) * $cal;

        }else if($this->pay_month >= 9){
            $result =  $cal - ($this->package->discount_9m/100) * $cal;
            $discount_sum =  ($this->package->discount_9m/100) * $cal;

        }
        else if($this->pay_month >= 12){
            $result = $cal -  ($this->package->discount_1y/100) * $cal;
            $discount_sum =  ($this->package->discount_1y/100) * $cal;

        }
      
        return ["price"=>$result,"discount_sum"=>$discount_sum];
    }
    
}
