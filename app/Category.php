<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name','img_url','branch_id'];

    public function types() {
        return $this->hasMany('App\Type');
    }

    public function foods(){
        return $this->hasManyThrough('App\Food','App\Type');
    }

}
