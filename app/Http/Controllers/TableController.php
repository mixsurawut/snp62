<?php

namespace App\Http\Controllers;

use App\Table;
use Illuminate\Http\Request;
use App\Bill;
class TableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tables = Table::where('branch_id',$request->user()->branch_id)->get();
        return $tables;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $req= $request->all();
        $req['branch_id'] = $request->user()->branch_id;
        return Table::create($req);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Table  $table
     * @return \Illuminate\Http\Response
     */
    public function show(Table $table)
    {
        return $table;
    }
    public function checkQrCode($qrcode)
    {
        $table = Table::where('qrcode',$qrcode)->whereNotNull('qrcode')->first();
        return $table;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Table  $table
     * @return \Illuminate\Http\Response
     */
    public function edit(Table $table)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Table  $table
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Table $table)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Table  $table
     * @return \Illuminate\Http\Response
     */
    public function destroy(Table $table)
    {
        //
    }

    public function checkBill(Request $request,$id){
        $table = Table::find($id);
        $table->update([
            'qrcode' => null
        ]);

        $bill = Bill::where('table_id',$table->id)->latest()->first();
        $bill->update(['status'=>1]);


        
        return $table;
    }
}
