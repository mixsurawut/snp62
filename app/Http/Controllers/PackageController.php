<?php

namespace App\Http\Controllers;

use App\Package;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $packages = Package::get();
        return $packages;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Package::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Package  $Package
     * @return \Illuminate\Http\Response
     */
    public function show(Package $Package)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Package  $Package
     * @return \Illuminate\Http\Response
     */
    public function edit(Package $Package)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Package  $Package
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Package $Package)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Package  $Package
     * @return \Illuminate\Http\Response
     */
    public function destroy(Package $Package)
    {
        //
    }
}
