<?php

namespace App\Http\Controllers;

use App\EmployeeBranch;
use Illuminate\Http\Request;

class EmployeeBranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeBranch  $employeeBranch
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeBranch $employeeBranch)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmployeeBranch  $employeeBranch
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeBranch $employeeBranch)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeeBranch  $employeeBranch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeBranch $employeeBranch)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeBranch  $employeeBranch
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeBranch $employeeBranch)
    {
        //
    }
}
