<?php

namespace App\Http\Controllers;

use App\StorePackage;
use Illuminate\Http\Request;

class StorePackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return StorePackage::get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return StorePackage::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StorePackage  $storePackage
     * @return \Illuminate\Http\Response
     */
    public function show(StorePackage $storePackage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StorePackage  $storePackage
     * @return \Illuminate\Http\Response
     */
    public function edit(StorePackage $storePackage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StorePackage  $storePackage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StorePackage $storePackage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StorePackage  $storePackage
     * @return \Illuminate\Http\Response
     */
    public function destroy(StorePackage $storePackage)
    {
        //
    }
}
