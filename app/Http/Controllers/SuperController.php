<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Branch;
use Carbon\Carbon;
class SuperController extends Controller
{
    public function bList(){
        $branches = Branch::get();

        foreach($branches as $b){
            $b->isExpire = $b->expiry_date < Carbon::now();
            $b->user;
            $b->package;
        }
        return $branches;
    }
   public function approveList(){
       $users = User::where('role_id',1)->whereNull('branch_id')->whereNotNull('store_package_id')->with('package')->where('pay_status',1)->get();
       return $users;
   }
   public function BapproveList(){
    $b = Branch::where('pay_status',1)->with('package')->get();
    return $b;
}

   public function approve(Request $request,$id){
    $user = User::find( $id);

    $branch = Branch::create([
        "name"=>$user->store_name,
        "description" =>'-',
        "expiry_date" => Carbon::now()->addDay(env('FREE_DAY',0)),
        "user_id" =>$user->id,
        "store_package_id"=>$user->store_package_id
    ]);

    $user->update([
        "pay_status"=>0,
        "pay_image_url"=>null,
        "branch_id"=>$branch->id,
        "role_id"=>2,

    ]);
    
    $u = request()->user();
    if($u->tel){
        $sms = new THSMS();

    $sms->send('NOTICE', $u->tel,'ทำการอนุมัติร้าน '.$branch->name.' ทดลองใช้ฟรี '.(env('FREE_DAY',0)).' วัน แล้ว หมดอายุ '.$branch->expiry_date_text);
        
    }
    return $user;
}

public function Bapprove(Request $request,$id){
    $b = Branch::find( $id);
    $ex;
    if($b->expiry_date < Carbon::now()){
       $ex = Carbon::now()->addMonth($b->pay_month);
    }else{
        $ex = Carbon::parse($b->expiry_date)->addMonth($b->pay_month);
 
    }
    $b->update([
        "expiry_date" =>$ex,
        "pay_status"=>0
    ]);

    $u = request()->user();
    if($u->tel){
        $sms = new THSMS();

    $sms->send('NOTICE', $u->tel,'ทำการต่อเวลา'.$b->paymonth.'เดือน เข้าร้าน'.$b->name.' แล้ว หมดอายุ '.$b->expiry_date_text);
        
    }

    return 'ok';
}
    
}

class THSMS
{
    var $api_url   = 'http://www.thsms.com/api/rest';
    var $username  = 'armkrit';
    var $password  = 'helloworld';

    public function getCredit()
    {
        $params['method']   = 'credit';
        $params['username'] = $this->username;
        $params['password'] = $this->password;

        $result = $this->curl($params);

        $xml = @simplexml_load_string($result);

        if (!is_object($xml)) {
            return array(FALSE, 'Respond error');
        } else {

            if ($xml->credit->status == 'success') {
                return array(TRUE, $xml->credit->status);
            } else {
                return array(FALSE, $xml->credit->message);
            }
        }
    }

    public function send($from = '0000', $to = null, $message = null)
    {
        $params['method']   = 'send';
        $params['username'] = $this->username;
        $params['password'] = $this->password;

        $params['from']     = $from;
        $params['to']       = $to;
        $params['message']  = $message;

        if (is_null($params['to']) || is_null($params['message'])) {
            return FALSE;
        }

        $result = $this->curl($params);
        $xml = @simplexml_load_string($result);
        if (!is_object($xml)) {
            return array(FALSE, 'Respond error');
        } else {
            if ($xml->send->status == 'success') {
                return array(TRUE, $xml->send->uuid);
            } else {
                return array(FALSE, $xml->send->message);
            }
        }
    }

    private function curl($params = array())
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->api_url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response  = curl_exec($ch);
        $lastError = curl_error($ch);
        $lastReq = curl_getinfo($ch);
        curl_close($ch);

        return $response;
    }
}