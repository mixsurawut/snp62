<?php

namespace App\Http\Controllers;

use App\FoodBill;
use App\Food;
use App\Table;
use App\Bill;
use Carbon\Carbon;

use Illuminate\Http\Request;

class FoodBillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $bills = FoodBill::whereHas('bill',function($q){
            $user = request()->user();
            $tables = Table::where('branch_id',$user->branch_id)->get()->pluck('id');
            $q->whereIn('table_id',$tables);
        })->whereDate('created_at', Carbon::today())->whereNull('checked_at')->with('food')->with('bill')->with('bill.table')->get();

        // foreach ($bill->lists as $list) {
        //     $list->food;
        // }
        return $bills;
    }

    public function check($id)
    {
        $food_bill = FoodBill::find($id);
        $food_bill->update([
            'checked_at' => Carbon::now()
        ]);


        $updateFood = Food::find($food_bill->food_id);
        $updateFood->decrement('unit',  $food_bill->amount);
        return  $food_bill;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $qr = $request->cookie('qrcode');
        $table = Table::where('qrcode', $qr)->latest()->first();
        $bill = Bill::where('table_id', $table->id)->latest()->first();

        foreach ($request->lists as $food) {

            //check ว่าเคย add รายการนี้เข้ามาแล้วหรือยัง ด้วย bill_id and food_id

            $food_bill = FoodBill::create([
                "bill_id" => $bill->id,
                "food_id" => $food['id'],
                "amount" => $food['input']
            ]);


            // $updateFood = Food::find($food['id']);
            // $updateFood->decrement('unit',  $food['input']);
        }


        return [[
            $table,
            $bill,
            "list" => $request->lists
        ]];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FoodBill  $foodBill
     * @return \Illuminate\Http\Response
     */
    public function show(FoodBill $foodBill)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FoodBill  $foodBill
     * @return \Illuminate\Http\Response
     */
    public function edit(FoodBill $foodBill)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FoodBill  $foodBill
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FoodBill $foodBill)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FoodBill  $foodBill
     * @return \Illuminate\Http\Response
     */
    public function destroy(FoodBill $foodBill)
    {
        //
    }
}