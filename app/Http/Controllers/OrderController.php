<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branch = request()->user()->branch_id;

        $categories = Category::where('branch_id',$branch)->get();
        foreach($categories as $category) {
            $category->foods;
        //    foreach($category->types as $type){
        //        $type->foods;
        //    }
        }

        return $categories;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $branch = request()->user()->branch_id;

        $data['branch_id'] =  $branch;
        $cat = Category::create($request->all());
        return $cat;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category,$id)
    {
            return Task::find($id);
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
        $cat = Category::find($id);
        
        return view('edit',compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id, Category $category)
    {
        $cat = Category::findOrFail($id);
        $cat->update($request->all());
        return $cat;
        //
    } 

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $id, Category $category)
    {
        $cat = Category::findOrFail($id);
        $cat->destroy();
        return 204;
        //
    }
}
