<?php

namespace App\Http\Controllers;

use App\Bill;
use App\FoodBill;
use App\Food;
use Carbon\Carbon;
use App\Table;
use Illuminate\Http\Request;

class DashBoardController extends Controller
{
    public function home(Request $request){
        $table_id = 0;

        $user = request()->user();
        
        $table = Table::where('qrcode',$request->qrcode)->where('branch_id', $user->branch_id)->first();
        if($table){
            $table_id = $table->id;
        }


        $foodBill = Bill::where('table_id',$table_id)->where('status',0)->with('package')->latest()->first();
        return  ["foodBill"=>$foodBill,"table"=>$table];
    }
    public function index(Request $request)
    {
        // รายได้วันนี้
        // จำนวนลูกค้า
        // จำนวนโต๊ะที่เปิดอยู่
        // รายการอาหารที่ถูกสั่งมากที่สุด
        $user = request()->user();

        $tables = Table::where('branch_id',    $user->branch_id)->pluck('id'); 
        $todaySumPrice = Bill::whereIn('table_id',  $tables)->whereDate('created_at',Carbon::now())->where('status',1)->with('package')->get();

        $today_sum  = 0;
        $today_people = 0;
        foreach($todaySumPrice as $p){
            $today_sum += ($p->people * $p->package->price);
            $today_people += $p->people;
        }

        $weekSumPrice = Bill::whereIn('table_id',  $tables)->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->where('status',1)->with('package')->get();

        $week_sum  = 0;
        foreach($weekSumPrice as $p){
            $week_sum += ($p->people * $p->package->price);
        }


        $monthSumPrice = Bill::whereIn('table_id',  $tables)->whereMonth('created_at',Carbon::now())->where('status',1)->with('package')->get();

        $month_sum  = 0;
        foreach($monthSumPrice as $p){
            $month_sum += ($p->people * $p->package->price);
        }



        $table_active = count(Bill::whereIn('table_id',  $tables)->where('status',0)->with('package')->get());

        $popular_food = FoodBill::whereDate('created_at',Carbon::now())->whereHas('bill',function($q) use ($tables){
           $q->whereIn('table_id',  $tables);
        })->get()->groupBy('food_id');

     
        $max_id = $max_sum = 0;
        foreach($popular_food as $index=> $pop){
            $suma = 0;
            foreach($pop as $p){
                $suma += $p->amount;
            }

            if($suma > $max_sum){
                $max_id = $index;
                $max_sum = $suma;

            }


        }

        $popFood = Food::find($max_id);



        return [
           "popfood"=>["food"=>$popFood,"amount"=>$max_sum],
           "today_sum"=>$today_sum,
           "week_sum"=> $week_sum,
           "month_sum" =>$month_sum,
           "today_people"=> $today_people,
           "table_active"=> $table_active,
          
        ];

    }
  
}