<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    protected $fillable = ['user_id', 'employee_id', 'table_id', 'promotion_id
    ', 'people', 'package_id','status'];


    public function lists()
    {
        return $this->hasMany('App\FoodBill');
    }
    public function table()
    {
        return $this->belongsTo('App\Table');
    }
    public function package(){
        return $this->belongsTo('App\Package');
    }
}