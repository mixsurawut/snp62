<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

Carbon::setLocale('th');

class FoodBill extends Model
{
    protected $fillable = ['bill_id', 'food_id', 'user_id', 'amount', 'checked_at'];
    protected $appends = ['timehuman'];
    public function food()
    {
        return $this->belongsTo('App\Food');
    }
    public function bill()
    {
        return $this->belongsTo('App\Bill');
    }

    public function getTimehumanAttribute()
    {
        return Carbon::parse($this->created_at)->diffForHumans();
    }
}