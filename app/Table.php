<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Bill;
class Table extends Model
{
    protected $fillable = ['qrcode','name','branch_id'];

    protected $appends = ['qrcode_url','price'];

    public function getQrcodeUrlAttribute(){
         $host = url('');
        return $this->qrcode ? 'https://api.qrserver.com/v1/create-qr-code/?data='.$host.'/orders?qrcode='.$this->qrcode : '';

    }
   
    public function getPriceAttribute(){
        $bill= Bill::where('table_id',$this->id)->where('status',0)->latest()->first();
        if($bill){
            return [
            "sum"=>$bill->people * $bill->package->price,
            "price" => $bill->package->price,
            "people"=>$bill->people,
            
        ];
        }
        else{
            return [
            "sum"=> 0,
            "price" => 0,
            "people"=> 0,
        ];
        }
    }
}
