<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    protected $fillable = ['name', 'point', 'price', 'unit', 'img_url', 'type_id', 'brand_id'];
    protected $appends = ['input', 'max'];
    public function getInputAttribute()
    {
        return 0;
    }

    public function getMaxAttribute()
    {
        return $this->type->max;
    }

    public function type()
    {
        return $this->belongsTo('App\Type');
    }
}