<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StorePackage extends Model
{
    protected $fillable = ['name', 'price', 'menu_limit', 'discount_3m', 'discount_6m', 'discount_9m', 'discount_1y'];

}
