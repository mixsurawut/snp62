<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $fillable = ['category_id','name','max'];
    public function foods() {
        return $this->hasMany('App\Food');
    }

    public function category() {
        return $this->belongsTo('App\Category');
    }

}
