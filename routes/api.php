<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Bill;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', 'Auth\LoginController@logout');

    Route::get('/user', function (Request $request) {

        $user = $request->user();

        // $bill= Bill::where('user_id',$user->id)->where('status',0)->latest()->with('table')->first();
        // $user->bill = $bill;
        $user->role;
        $user->branch;
        $user->isExpire = false;

        if($user->role_id == 2 || $user->role_id == 3|| $user->role_id == 4){
            $user->isExpire = $user->branch->expiry_date < Carbon::now();

        }
        

        return $user;
    });

    Route::resource('tables', 'TableController');



    Route::resource('branchs', 'BranchController');
    Route::resource('packages', 'PackageController');
    Route::resource('promotions', 'PromotionController');
    Route::resource('bills', 'BillController');

    Route::patch('settings/profile', 'Settings\ProfileController@update');
    Route::patch('settings/password', 'Settings\PasswordController@update');
});

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('login', 'Auth\LoginController@login');
    Route::post('register', 'Auth\RegisterController@register');
    Route::post('register_new', 'Auth\RegisterController@createDraft');

    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::post('email/verify/{user}', 'Auth\VerificationController@verify')->name('verification.verify');
    Route::post('email/resend', 'Auth\VerificationController@resend');

    Route::post('oauth/{driver}', 'Auth\OAuthController@redirectToProvider');
    Route::get('oauth/{driver}/callback', 'Auth\OAuthController@handleProviderCallback')->name('oauth.callback');
});
Route::resource('orders', 'OrderController');
Route::get('orders/{id}', 'OrderController@show');
Route::post('orders', 'OrderController@store');
Route::put('orders/{id}', 'OrderController@update');
Route::delete('orders/{id}', 'OrderController@delete');

Route::get('tables/check_qrcode/{qrcode}', 'TableController@checkQrCode');
Route::post('uploadImage', 'UploadController@uploadImage');
Route::post('uploadFile', 'UploadController@uploadFile');

Route::resource('brands', 'BrandController');
Route::resource('types', 'TypeController');
Route::resource('foods', 'FoodController');
Route::resource('bills', 'BillController');
Route::resource('food_bills', 'FoodBillController');
Route::post('food_bill/{id}/check', 'FoodBillController@check');
Route::post('checkbill/{id}', 'TableController@checkBill');
Route::get('dashboard', 'DashBoardController@index');
Route::get('home', 'DashBoardController@home');
Route::resource('roles', 'RoleController');
Route::post('register_new2', 'RoleController@newUser');

Route::resource('store_package', 'StorePackageController');

Route::post('newpassword', 'Settings\ProfileController@newPassword');


Route::group(['prefix'=>'superadmin','as'=>'superadmin.'], function(){

    Route::get('branches', 'SuperController@bList');

    Route::get('confirmcustomer', 'SuperController@approveList');
    Route::post('confirmcustomer/{id}', 'SuperController@approve');

    Route::get('confirmcustomer_renew', 'SuperController@BapproveList');
    Route::post('confirmcustomer_renew/{id}', 'SuperController@Bapprove');


    
    });
// superadmin

