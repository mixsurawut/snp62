// auth.js
export const LOGOUT = "LOGOUT";
export const SAVE_TOKEN = "SAVE_TOKEN";
export const FETCH_USER = "FETCH_USER";
export const FETCH_USER_SUCCESS = "FETCH_USER_SUCCESS";
export const FETCH_USER_FAILURE = "FETCH_USER_FAILURE";
export const UPDATE_USER = "UPDATE_USER";

// lang.js
export const SET_LOCALE = "SET_LOCALE";

// order.js
export const FETCH_ORDER = "FETCH_ORDER";
export const FETCH_ORDER_LIST = "FETCH_ORDER_LIST";
export const SET_ORDER_INPUT = "SET_ORDER_INPUT";

export const FETCH_TABLE = "FETCH_TABLE";
export const FETCH_TABLE_SHOW = "FETCH_TABLE_SHOW";
export const FETCH_BRANCH = "FETCH_BRANCH";
export const FETCH_PACKAGE = "FETCH_PACKAGE";
export const FETCH_PROMOTION = "FETCH_PROMOTION";
export const FETCH_BRAND = "FETCH_BRAND";
export const FETCH_TYPE = "FETCH_TYPE";
export const FETCH_FOODBILL = "FETCH_FOODBILL";
export const FETCH_BILL = "FETCH_BILL";
export const FETCH_DASHBORD = "FETCH_DASHBORD";
export const FETCH_SPACK = "FETCH_SPACK";


export const SET_QRCODE = "SET_QRCODE";
