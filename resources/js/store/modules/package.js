import axios from 'axios'
import * as types from '../mutation-types'

// state
export const state = {
  packages: null

}

// getters
export const getters = {
  packages: state => state.packages
}

// mutations
export const mutations = {
  [types.FETCH_PACKAGE] (state, data) {
    state.packages = data
  }
}

// actions
export const actions = {
  async fetch ({ commit }) {
    try {
      const { data } = await axios.get(`/api/packages`)
      commit(types.FETCH_PACKAGE, data)
    } catch (e) {
      console.log(e)
    }
  }
}
