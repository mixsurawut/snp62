import axios from 'axios'
import * as types from '../mutation-types'

// state
export const state = {
  bills: null
}

// getters
export const getters = {
  bills: state => state.bills
}

// mutations
export const mutations = {
  [types.FETCH_BILL] (state, data) {
    state.bills = data
  }
}

// actions
export const actions = {
  async fetch ({ commit }) {
    try {
      const { data } = await axios.get(`/api/bills`)
      commit(types.FETCH_BILL, data)
    } catch (e) {
      console.log(e)
    }
  }
}
