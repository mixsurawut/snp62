import axios from 'axios'
import * as types from '../mutation-types'

// state
export const state = {
  branchs: null
}

// getters
export const getters = {
  branchs: state => state.branchs
}

// mutations
export const mutations = {
  [types.FETCH_BRANCH] (state, data) {
    state.branchs = data
  }
}

// actions
export const actions = {
  async fetch ({ commit }) {
    try {
      const { data } = await axios.get(`/api/branchs`)
      commit(types.FETCH_BRANCH, data)
    } catch (e) {
      console.log(e)
    }
  }
}
