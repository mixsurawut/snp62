import axios from 'axios'
import * as types from '../mutation-types'

// state
export const state = {
  tables: null,
  show: null
}

// getters
export const getters = {
  tables: state => state.tables,
  show: state => state.show
}

// mutations
export const mutations = {
  [types.FETCH_TABLE] (state, data) {
    state.tables = data
  },
  [types.FETCH_TABLE_SHOW] (state, data) {
    state.show = data
  }

}

// actions
export const actions = {
  async fetch ({ commit }, id = 1) {
    try {
      const { data } = await axios.get(`/api/tables?branch_id=${id}`)
      commit(types.FETCH_TABLE, data)
    } catch (e) {
      console.log(e)
    }
  },
  async show ({ commit }, id = 1) {
    try {
      const { data } = await axios.get(`/api/tables/${id}`)
      commit(types.FETCH_TABLE_SHOW, data)
    } catch (e) {
      console.log(e)
    }
  }
}
