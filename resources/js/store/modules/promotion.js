import axios from 'axios'
import * as types from '../mutation-types'

// state
export const state = {
  promotions: null

}

// getters
export const getters = {
  promotions: state => state.promotions
}

// mutations
export const mutations = {
  [types.FETCH_PROMOTION] (state, data) {
    state.promotions = data
  }
}

// actions
export const actions = {
  async fetch ({ commit }) {
    try {
      const { data } = await axios.get(`/api/promotions`)
      commit(types.FETCH_PROMOTION, data)
    } catch (e) {
      console.log(e)
    }
  }
}
