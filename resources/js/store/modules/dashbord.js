import axios from 'axios'
import * as types from '../mutation-types'

// state
export const state = {
  dashbords: null
}

// getters
export const getters = {
  dashbords: state => state.dashbords
}

// mutations
export const mutations = {
  [types.FETCH_DASHBORD] (state, data) {
    state.dashbords = data
  }
}

// actions
export const actions = {
  async fetch ({ commit }) {
    try {
      const { data } = await axios.get(`/api/dashboard`)
      commit(types.FETCH_DASHBORD, data)
    } catch (e) {
      console.log(e)
    }
  }
}
