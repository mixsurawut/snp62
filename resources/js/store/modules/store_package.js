import axios from 'axios'
import * as types from '../mutation-types'

// state
export const state = {
  items: null,
  show: null
}

// getters
export const getters = {
    items: state => state.items,
  show: state => state.show
}

// mutations
export const mutations = {
  [types.FETCH_SPACK] (state, data) {
    state.items = data
  },
  [types.FETCH_TABLE_SHOW] (state, data) {
    state.show = data
  }

}

// actions
export const actions = {
  async fetch ({ commit }) {
    try {
      const { data } = await axios.get(`/api/store_package`)
      commit(types.FETCH_SPACK, data)
    } catch (e) {
      console.log(e)
    }
  },
  async show ({ commit }, id = 1) {
    try {
      const { data } = await axios.get(`/api/tables/${id}`)
      commit(types.FETCH_TABLE_SHOW, data)
    } catch (e) {
      console.log(e)
    }
  }
}
