import axios from "axios";
import * as types from "../mutation-types";

// state
export const state = {
  items: null
};

// getters
export const getters = {
  items: state => state.items
};

// mutations
export const mutations = {
  [types.FETCH_FOODBILL](state, data) {
    state.items = data;
  }
};

// actions
export const actions = {
  async fetch({ commit }) {
    try {
      const { data } = await axios.get(`/api/food_bills`);
      commit(types.FETCH_FOODBILL, data);
    } catch (e) {
      console.log(e);
    }
  }
};
