import axios from 'axios'
import * as types from '../mutation-types'
import Cookies from 'js-cookie'
// state
export const state = {
  orders: null,
  lists: null,
  qrcode: Cookies.get('qrcode')

}

// getters
export const getters = {
  orders: state => state.orders,
  lists: state => state.lists,
  qrcode: state => state.qrcode
}

// mutations
export const mutations = {
  [types.FETCH_ORDER] (state, data) {
    state.orders = data
  },
  [types.FETCH_ORDER_LIST] (state, data) {
    state.lists = data
  },
  [types.SET_QRCODE] (state, { qrcode }) {
    state.qrcode = qrcode
  }
}

// actions
export const actions = {
  setQR ({ commit }, { qrcode }) {
    commit(types.SET_QRCODE, { qrcode })

    Cookies.set('qrcode', qrcode, { expires: 365 })
  },
  async fetch ({ commit }) {
    try {
      const { data } = await axios.get('/api/orders')
      commit(types.FETCH_ORDER, data)
    } catch (e) {
      console.log(e)
    }
  },
  async addList ({ commit }, data) {
    commit(types.FETCH_ORDER_LIST, data)
  }
}
