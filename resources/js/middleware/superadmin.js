import store from '~/store'

export default (to, from, next) => {
  if (store.getters['auth/user'].role.name !== 'superadmin' ) {
    next({ name: 'home' })
  } else {
    next()
  }
}
