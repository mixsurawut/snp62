function page(path) {
  return () =>
    import(/* webpackChunkName: '' */ `~/pages/${path}`).then(
      m => m.default || m
    );
}

export default [
  { path: "/", name: "welcome", component: page("welcome.vue") },

  { path: "/login", name: "login", component: page("auth/login.vue") },
  { path: "/register", name: "register", component: page("auth/register.vue") },
  {
    path: "/password/reset",
    name: "password.request",
    component: page("auth/password/email.vue")
  },
  {
    path: "/password/reset/:token",
    name: "password.reset",
    component: page("auth/password/reset.vue")
  },
  {
    path: "/email/verify/:id",
    name: "verification.verify",
    component: page("auth/verification/verify.vue")
  },
  {
    path: "/email/resend",
    name: "verification.resend",
    component: page("auth/verification/resend.vue")
  },

  { path: "/test", name: "test.test", component: page("admin/test/test.vue") },

  { path: "/home", name: "home", component: page("home.vue") },
  { path: "/getting-started", name: "getting-started", component: page("getting-started.vue") },
  { path: "/createnewtemplate", name: "createnewtemplate", component: page("createnewtemplate.vue") },
  { path: "/newpassword", name: "newpassword", component: page("newpassword.vue") },
  { path: "/orders", name: "order", component: page("order/order.vue") },
  {
    path: "/orders/lists",
    name: "order.list",
    component: page("order/order-list.vue")
  },
  {
    path: "/orders/confirm",
    name: "order.confirm",
    component: page("order/confirm.vue")
  },
  { path: "/tables", name: "table", component: page("table/table.vue") },
  {
    path: "/tables/:id",
    name: "table.registertable",
    component: page("table/registertable.vue")
  },

  {
    path: "/superadmin",
    component: page("superadmin/index.vue"),
    children: [
      { path: "", name: "superadmin", component: page("superadmin/dashboard.vue") },
      {
        path: "dashboards",
        name: "superadmin.dashboard",
        component: page("superadmin/dashboard.vue")
      },
      {
        path: "confirmcustomer",
        name: "superadmin.confirmcustomer",
        component: page("superadmin/confirmcustomer.vue")
      },
      {
        path: "package",
        name: "superadmin.package",
        component: page("superadmin/package.vue")
      },
      {
        path: "package_list",
        name: "superadmin.package_list",
        component: page("superadmin/package_list.vue")
      },
      {
        path: "confirmcustomer_renew",
        name: "superadmin.confirmcustomer_renew",
        component: page("superadmin/confirmcustomer_renew.vue")
      },
      {
        path: "details/:id",
        name: "superadmin.details",
        component: page("superadmin/details.vue")
      },
    ]
  },

  {
    path: "/settings",
    component: page("settings/index.vue"),
    children: [
      { path: "", redirect: { name: "settings.profile" } },
      {
        path: "profile",
        name: "settings.profile",
        component: page("settings/profile.vue")
      },
      {
        path: "password",
        name: "settings.password",
        component: page("settings/password.vue")
      }
    ]
  },
  {
    path: "/admin",
    component: page("admin/index.vue"),
    children: [
      { path: "", name: "admin", component: page("admin/dashboard.vue") },
      {
        path: "dashboards",
        name: "admin.dashboard",
        component: page("admin/dashboard.vue")
      },
      {
        path: "package_renew",
        name: "admin.package_renew",
        component: page("admin/package_renew.vue")
      },
      {
        path: "foods/create",
        name: "admin.food.create",
        component: page("admin/food/form.vue")
      },
      {
        path: "foods/:id/edit",
        name: "admin.food.edit",
        component: page("admin/food/form.vue")
      },
      {
        path: "foods/:id",
        name: "admin.food.update",
        component: page("admin/food/updateform.vue")
      },
      {
        path: "foods",
        name: "admin.food",
        component: page("admin/food/index.vue")
      },
      {
        path: "stocks",
        name: "admin.stock",
        component: page("admin/stock.vue")
      },
      {
        path: "addtypes",
        name: "admin.addtype",
        component: page("admin/addtype.vue")
      },
      {
        path: "addfoods",
        name: "admin.addfood",
        component: page("admin/addfood.vue")
      },
   
      {
        path: "edittypes",
        name: "admin.edittype",
        component: page("admin/edittype.vue")
      },
      {
        path: "editfoods",
        name: "admin.editfood",
        component: page("admin/editfood.vue")
      },
      {
        path: "typefood",
        name: "admin.typefood",
        component: page("admin/typefood.vue")
      },
      {
        path: "addtable",
        name: "admin.addtable",
        component: page("admin/addtable.vue")
      },
      {
        path: "edittable",
        name: "admin.edittable",
        component: page("admin/edittable.vue")
      },
      {
        path: "roleadmin",
        name: "admin.roleadmin",
        component: page("admin/roleadmin.vue")
      },
      {
        path: "employee",
        name: "admin.employee",
        component: page("admin/employee.vue")
      },
      {
        path: "basicsetting",
        name: "admin.basicsetting",
        component: page("admin/basicsetting.vue")
      },
      
      {
        path: "confirmorder",
        name: "admin.confirmorder",
        component: page("admin/confirmorder.vue")
      }
    ]
  },

  { path: "*", component: page("errors/404.vue") }
];
