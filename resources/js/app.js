/* eslint-disable import/first */
import Vue from "vue";
import store from "~/store";
import router from "~/router";
import i18n from "~/plugins/i18n";
import App from "~/components/App";
import moment from 'moment'

Vue.prototype.moment = moment
import "~/plugins";
import "~/components";
import BootstrapVue from "bootstrap-vue";
// ES6 Modules or TypeScript
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'

Vue.use(VueMaterial)

Vue.use(BootstrapVue);
Vue.config.productionTip = false;

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";



import Mixins from "./mixins";

Vue.mixin(Mixins);
/* eslint-disable no-new */
new Vue({
  i18n,
  store,
  router,
  ...App
});
