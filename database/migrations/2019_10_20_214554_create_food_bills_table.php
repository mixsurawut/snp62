<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoodBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('food_bills', function (Blueprint $table) {
            $table->bigIncrements('id');
            // fk
            $table->bigInteger('bill_id')->unsigned()->default(1);
            $table->foreign('bill_id')
                ->references('id')
                ->on('bills')
                ->onDelete('cascade');

            // fk
            $table->bigInteger('food_id')->unsigned()->default(1);
            $table->foreign('food_id')
                ->references('id')
                ->on('foods')
                ->onDelete('cascade');

            //fk
            $table->bigInteger('user_id')->unsigned()->default(1);
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->timestamp('checked_at')->nullable();

            $table->integer('amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('food_bills');
    }
}