<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePacketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('price');

            // fk    
             $table->bigInteger('branch_id')->unsigned()->default(1);
            $table->foreign('branch_id')
            ->references('id')
            ->on('branches')
            ->onDelete('cascade');
            $table->timestamps();
        });

        Schema::table('bills', function(Blueprint $table){
            $table->bigInteger('package_id')->unsigned()->default(1);
            $table->foreign('package_id')
            ->references('id')
            ->on('packages')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
