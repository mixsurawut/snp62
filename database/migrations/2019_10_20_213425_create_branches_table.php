<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('description');
            $table->date('expiry_date')->nullable();
            $table->bigInteger('user_id')->nullable()->default(1);
            $table->bigInteger('store_package_id')->nullable();
            $table->string('pay_image_url')->nullable();
            $table->integer('pay_month')->default(1);
            $table->integer('pay_status')->comment('0.no 1.wait 2.no approve')->default(0);
            $table->timestamps();
            $table->string('color_1')->default("#f96c00");
            $table->string('color_2')->default("#FFFFFF");
            $table->string('img_logo_url')->default("/images/logo.png");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branches');
    }
}
