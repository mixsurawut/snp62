<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('branch_id')->nullable();
            $table->string('first_name');
            $table->string('last_name');
            $table->enum('gender', ['m', 'f'])->comment('m = male , f = female');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('new_password')->default(0);

            $table->string('tel')->nullable();

            $table->string('store_name')->nullable();
            $table->bigInteger('store_package_id')->nullable();
            $table->string('pay_image_url')->nullable();
            $table->integer('pay_status')->comment('0.no 1.wait 2.no approve')->default(1);

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
