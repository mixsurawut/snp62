<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('status')->default(0)->comment('0.on table 1.checkbill');

         
            $table->bigInteger('user_id')->unsigned()->default(1);
            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');

             // fk
            $table->bigInteger('employee_id')->unsigned()->default(1);
            $table->foreign('employee_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');

             // fk
            $table->bigInteger('table_id')->unsigned()->default(1);
            $table->foreign('table_id')
            ->references('id')
            ->on('tables')
            ->onDelete('cascade');

             // fk
            $table->bigInteger('promotion_id')->nullable();
            // $table->foreign('promotion_id')
            // ->references('id')
            // ->on('promotions')
            // ->onDelete('cascade');

           
            
            $table->integer('people');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills');
    }
}
