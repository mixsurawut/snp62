<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('employee_branches', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //      // fk
        //     $table->bigInteger('employee_id')->unsigned()->default(1);
        //     $table->foreign('employee_id')
        //     ->references('id')
        //     ->on('users')
        //     ->onDelete('cascade');

        //      // fk
        //     $table->bigInteger('branch_id')->unsigned()->default(1);
        //     $table->foreign('branch_id')
        //     ->references('id')
        //     ->on('branches')
        //     ->onDelete('cascade');


        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_branches');
    }
}
