<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('point');
            $table->integer('price');
            $table->integer('unit');
            
            // fk
            $table->bigInteger('type_id')->unsigned()->default(1);
            $table->foreign('type_id')
            ->references('id')
            ->on('types')
            ->onDelete('cascade');

            // fk
            $table->bigInteger('brand_id')->unsigned()->default(1);
            $table->foreign('brand_id')
            ->references('id')
            ->on('brands')
            ->onDelete('cascade');
            
            $table->string('img_url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foods');
    }
}
