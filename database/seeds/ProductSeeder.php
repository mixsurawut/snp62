<?php

use Illuminate\Database\Seeder;
use App\Category;
use App\Type;
use App\Food;
use PhpParser\Node\Expr\Isset_;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name'=>'เนื้อสัตว์',
                'img_url'=>'/images/categories/meat/pork/mixed_pork_set.jpg',
                'types' => [
                    [
                        'name' => 'เนื้อหมู',
                        'img_url'=>'/images/categories/meat/pork/pork_tenderloin.jpg',
                        
                        'foods' => [
                            [
                                'name' => 'สันคอหมูสไลด์',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/meat/pork/pork_tenderloin.jpg',
                                'brand_id' => 1
                            ],
                            [
                                'name' => 'สันนอกหมูสไลด์',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/meat/pork/pork_tenderloin2.jpg',
                                'brand_id' => 1
                            ],
                            [
                                'name' => 'สามชั้นสไลด์',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/meat/pork/three_layer_slide.jpg',
                                'brand_id' => 1
                            ],
                            [
                                'name' => 'ชุดหมูรวม',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/meat/pork/mixed_pork_set.jpg',
                                'brand_id' => 1
                            ],
                            [
                                'name' => 'หมูหมักนุ่ม',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/meat/pork/tender_marinated_pork.jpg',
                                'brand_id' => 1
                            ],
                        ]
                    ],
                        [
                        'name' => 'เนื้อไก่',
                        'img_url'=>'/images/categories/meat/chicken/chicken_slides.jpg',
                        'foods' => [
                            [
                                'name' => 'ไก่สไลด์',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/meat/chicken/chicken_slides.jpg',
                                'brand_id' => 1
                            ],
                            [
                                'name' => 'ไก่หมักนุ่ม',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/meat/chicken/tender_marinated_chicken.jpg',
                                'brand_id' => 1
                            ],
                        ]
                    ],
                    [
                        'name' => 'อาหารทะเล',
                        'img_url'=>'/images/categories/meat/seafood/dolly_fish.jpg',
                        'foods' => [
                            [
                                'name' => 'เนื้อปลาดอลลี่',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/meat/seafood/dolly_fish.jpg',
                                'brand_id' => 1
                            ],
                            [
                                'name' => 'ปลาหมึกสด',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/meat/seafood/fresh_squid.jpg',
                                'brand_id' => 1
                            ],
                             [
                                'name' => 'ปลาหมึกกรอบ',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/meat/seafood/crispy_squid.jpg',
                                'brand_id' => 1
                            ],
                            [
                                'name' => 'หอยแมลงภู่นิวซีแลนด์',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/meat/seafood/new_zealand_mussels.jpg',
                                'brand_id' => 1
                            ],
                            [
                                'name' => 'กุ้งสด',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/meat/seafood/fresh_shrimp.jpg',
                                'brand_id' => 1
                            ],
                        ]
                    ],

[
                        'name' => 'อาหารอื่นๆ',
                        'img_url'=>'/images/categories/meat/other_food/fish_tofu.jpg',
                        'max'=> 4,

                        'foods' => [
                            [
                                'name' => 'เต้าหู้ปลา',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/meat/other_food/fish_tofu.jpg',
                                'brand_id' => 1
                            ],
                            [
                                'name' => 'ลูกชิ้นปลา',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/meat/other_food/fish_ball.jpg',
                                'brand_id' => 1
                            ],
                             [
                                'name' => 'เกี๊ยวกุ้ง',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/meat/other_food/shrimp_dumplings.jpg',
                                'brand_id' => 1
                            ],
                            [
                                'name' => 'ปูอัดวาซาบิ',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/meat/other_food/wasabi_crab_sticks.jpg',
                                'brand_id' => 1
                            ],
                            [
                                'name' => 'ไข่ไก่',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/meat/other_food/egg.jpg',
                                'brand_id' => 1
                            ],
                            [
                                'name' => 'วุ้นเส้น',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/meat/other_food/vermicelli.jpg',
                                'brand_id' => 1
                            ],
                            [
                                'name' => 'บะหมี่หยก',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/meat/other_food/jade_noodle.jpg',
                                'brand_id' => 1
                            ],
                        ]
                    ],
                ]
            ],
            [
                'name'=>'ผัก',
                'img_url'=>'/images/categories/vegetable/vegetable/mixed_vegetable.jpg',
                'types' => [
                    [
                        'name' => 'ผัก',
                        'img_url'=>'/images/categories/vegetable/vegetable/mixed_vegetable.jpg',

                         'foods' => [
                            [
                                'name' => 'ผักรวม',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/vegetable/vegetable/mixed_vegetable.jpg',
                                'brand_id' => 1
                            ],
                            [
                                'name' => 'ผักกาดขาว',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/vegetable/vegetable/chinese_cabbage.jpg',
                                'brand_id' => 1
                            ],
                            [
                                'name' => 'กะหล่ำปลี',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/vegetable/vegetable/cabbage.jpg',
                                'brand_id' => 1
                            ],
                             [
                                'name' => 'ผักขาวกวางตุ้งฮ่องเต้',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/vegetable/vegetable/cantonese_white_vegetable.jpg',
                                'brand_id' => 1
                            ],
                            [
                                'name' => 'ผักบุ้ง',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/vegetable/vegetable/morning_glory.jpg',
                                'brand_id' => 1
                            ],
                            [
                                'name' => 'ต้นหอม',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/vegetable/vegetable/spring_onion.jpg',
                                'brand_id' => 1
                            ],
                            [
                                'name' => 'ขึ้นฉ่าย',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/vegetable/vegetable/celery.jpg',
                                'brand_id' => 1
                            ],
                            [
                                'name' => 'เห็ดออรินจิ',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/vegetable/vegetable/origin_mushroom.jpg',
                                'brand_id' => 1
                            ],
                            [
                                'name' => 'เห็ดเข็มทอง',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/vegetable/vegetable/mushroom.jpg',
                                'brand_id' => 1
                            ],
                            [
                                'name' => 'ข้าวโพด',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/vegetable/vegetable/corn.jpg',
                                'brand_id' => 1
                            ],
                        ]
                    ]
                ]
            ],
             [
                'name'=>'เครื่องดื่ม',
                'img_url'=>'/images/categories/beverage/beverage/drinking_water.jpg',
                'types' => [
                    [
                        'name' => 'เครื่องดื่ม',
                        'img_url'=>'/images/categories/beverage/beverage/drinking_water.jpg',

                         'foods' => [
                            [
                                'name' => 'น้ำเปล่า',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/beverage/beverage/drinking_water.jpg',
                                'brand_id' => 1
                            ],
                            [
                                'name' => 'น้ำแข็ง',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/beverage/beverage/ice.jpg',
                                'brand_id' => 1
                            ],
                            [
                                'name' => 'แปปซี่',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/beverage/beverage/pepsi.jpg',
                                'brand_id' => 1
                            ],
                            [
                                'name' => 'ลิ้นจี่',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/beverage/beverage/lychee_juice.jpg',
                                'brand_id' => 1
                            ],
                             [
                                'name' => 'ชาเขียว',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/beverage/beverage/green_tea.jpg',
                                'brand_id' => 1
                            ],
                             [
                                'name' => 'ชามะนาว',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/beverage/beverage/lemon_tea.jpg',
                                'brand_id' => 1
                            ],
                        ]
                    ]
                ]
            ],
                      [
                'name'=>'ของหวาน',
                'img_url'=>'/images/categories/dessert/ice_cream/chocolate_ice_cream.jpg',
                'types' => [
                    [
                        'name' => 'ไอศครืม',
                        'img_url'=>'/images/categories/dessert/ice_cream/chocolate_ice_cream.jpg',

                         'foods' => [
                            [
                                'name' => 'ไอศครืมช็อกโกแลต',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/dessert/ice_cream/chocolate_ice_cream.jpg',
                                'brand_id' => 1
                            ],
                            [
                                'name' => 'ไอศครืมชาเขียว',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/dessert/ice_cream/green_tea_ice_cream.jpg',
                                'brand_id' => 1
                            ],
                            [
                                'name' => 'ไอศครืมวนิลา',
                                'point' => 100,
                                'price' => 100,
                                'unit' => 100,
                                'img_url'=>'/images/categories/dessert/ice_cream/vanilla_ice_cream.jpg',
                                'brand_id' => 1
                            ],
                        ]
                    ]
                ]
            ],
        ];

        
        foreach($categories as $cat){
            $category = Category::create([
                'name'=> $cat['name'],
                'img_url' =>$cat['img_url']
            ]);


            foreach($cat['types'] as $type){
                $typex = Type::create([
                    'name' => $type['name'],
                    'img_url' => $type['img_url'],
                    'category_id'=> $category->id
                ]);


            if(isset($type['foods'])){
                foreach($type['foods'] as $food){
                                $foodx = Food::create([
                                    'name' => $food['name'],
                                    'img_url' => $food['img_url'],
                                    'point' => $food['point'],
                                    'price' => $food['price'],
                                    'unit' => $food['unit'],
                                    'brand_id' => $food['brand_id'],
                                    'type_id'=> $typex->id
                                ]);
                }
            }
          


            }

            

         
        
        }

       
    }
}
