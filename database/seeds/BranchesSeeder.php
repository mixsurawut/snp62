<?php

use Illuminate\Database\Seeder;
use App\Branches;
use Carbon\Carbon;

class BranchesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          DB::table('branches')->insert([
            // 1
            [
                
                'name' => 'สาขาบางใหญ่',
                'description' => 'lorem',
                "expiry_date" => '2020-04-03',
                "user_id"=>1,
                "store_package_id"=>1
            ],
            [
                
                'name' => 'สาขาพระราม2',      
                'description' => 'lorem',  
                "expiry_date" =>'2020-04-03',
                "user_id"=>1,
                "store_package_id"=>1


            ],
           
        ]);
    }
}
