<?php

use Illuminate\Database\Seeder;

class StorePackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('store_packages')->insert([
            [
                "name"=>'.Starter (1,700 บาท / เดือน)',
                "price"=>1700,
                "menu_limit"=>150,
                "discount_3m"=>0,
                "discount_6m"=>0,
                "discount_9m"=>0,
                "discount_1y"=>0,
            ],
            [
                "name"=>'.Professional (2,500 บาท / เดือน)',
                "price"=>2500,
                "menu_limit"=>200,
                "discount_3m"=>0,
                "discount_6m"=>0,
                "discount_9m"=>0,
                "discount_1y"=>0,
            ],
            [
                "name"=>'.Exclusive (2,700 บาท / เดือน)',
                "price"=>2700,
                "menu_limit"=>300,
                "discount_3m"=>0,
                "discount_6m"=>0,
                "discount_9m"=>0,
                "discount_1y"=>0,
            ],

        ]);
    }
}
