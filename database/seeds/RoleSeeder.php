<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            // 1
            [
                'name' => 'member'
            ],
            // 2
            [
                'name' => 'admin'
            ],
            // 3
            [
                'name' => 'employer'
            ],
            // 4
            [
                'name' => 'employee'
            ],
            // 5
            [
                'name' => 'superadmin'
            ],
        ]);
    }
}
