<?php

use Illuminate\Database\Seeder;

class PackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('packages')->insert([
            [
                'name' => 'ราคา199',
                'price' =>199,
                'branch_id' => 1,
            ],
            [
                'name' => 'ราคา299',
                'price' =>299,
                'branch_id' => 1,
            ],
            [
                'name' => 'ราคา399',
                'price' =>399,
                'branch_id' => 1,
            ],
            [
                'name' => 'ราคา499',
                'price' =>499,
                'branch_id' => 1,
            ],
        ]);
    }
}
