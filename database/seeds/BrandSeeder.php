<?php

use Illuminate\Database\Seeder;
use App\Brand;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Brand::insert([
            // 1
            [
                
                'name' => 'betagro',
                'img_url' => 'https://lh3.googleusercontent.com/proxy/EOTZN5RY16aGmhM9QTxe-r5chzRkEOMb4_Vlh0GYBLtTt9wzvT2iXdeSWlmaITbpzUzX36Scgawv_JRzFDCVujHHC8GpDPPfqo2ivIW1qS6b4ZJQlLq5GNEcsIuVSEIk'
            
            ],
            [
                
                'name' => 'cp-all',
                'img_url' => 'https://upload.wikimedia.org/wikipedia/th/3/39/CPALL2015.png'
            
            ]
        ]);
    }
}
