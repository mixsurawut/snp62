<?php

use Illuminate\Database\Seeder;

class PromotionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('promotions')->insert([
            // 1
            [
                
                'name' => 'ลดราคา 10 %',
                'img_url' => '/images/promotions/10%.jpg',        
            ],
            [
                
                'name' => 'ลดราคา 20 %',
                'branch_id' => '/images/promotions/20%.jpg',         
            ],
            [
                
                'name' => 'ลดราคา 30 %',
                'branch_id' => '/images/promotions/30%.jpg',         
            ],
            [
                
                'name' => 'ลดราคา 40 %',
                'branch_id' => '/images/promotions/40%.jpg',         
            ],
        ]);
    }
}
