<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
             [
                'id' => 1, 
                'first_name' => 'unknow',
                'last_name' =>'user',
                'password' => bcrypt('123456'),
                'gender' => 'm',
                'email' => 'mix@hotmail.com',
                'tel' => '0888888885',
                'role_id' => 1,
                'new_password' => 1,
                'branch_id'=> null,
            ],
            [
                'id' => 2, 
                'first_name' => 'admin',
                'last_name' =>'admin',
                'password' => bcrypt('123456'),
                'gender' => 'm',
                'email' => 'mixdk@hotmail.com',
                'tel' => '0888888888',
                'role_id' => 2,
                'new_password' => 1,
                'branch_id'=> 1,
            ],
            [
                'id' => 3, 
                'first_name' => 'superadmin',
                'last_name' =>'shuba',
                'password' => bcrypt('123456'),
                'gender' => 'm',
                'email' => 'admin@shuba.com',
                'tel' => '0875822477',
                'role_id' => 5,
                'new_password' => 1,
                'branch_id'=> null,
            ]
        ]);
        
        $faker = Faker\Factory::create();

        for($i = 1 ; $i<30 ; $i++){
            $ramdomStatus = rand(0,2);

            DB::table('users')->insert([
                [
                   'first_name' => $faker->firstName,
                   'last_name' => $faker->lastName,
                   'password' => bcrypt('123456'),
                   'gender' => 'm',
                   'email' => 'user'.$i.'@hotmail.com',
                   'tel' => $faker->e164PhoneNumber,
                   'role_id' => 1,
                   'new_password' => 1,
                   'store_package_id'=> rand(1,3),
                   'store_name'=> $faker->state,
                   'pay_image_url'=> $ramdomStatus == 1 ? 'https://f.ptcdn.info/650/037/000/ny8bp73dp2RePCW0h9R-o.jpg' : null,
                   'pay_status'=>$ramdomStatus
               ]
           ]);
        }

  

      
    }
}
