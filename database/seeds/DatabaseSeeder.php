<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call([
            StorePackageSeeder::class,
             RoleSeeder::class,
             UserSeeder::class,
             BrandSeeder::class,
             ProductSeeder::class,
             BranchesSeeder::class,
             TableSeeder::class,
             PackageSeeder::class,
             PromotionSeeder::class,
         ]);
    }
}
