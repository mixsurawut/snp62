<?php

use Illuminate\Database\Seeder;
use App\Table;
class TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tables')->insert([
            // 1
            [
                
                'name' => 'โต๊ะ1',
                'branch_id' => 1,        
            ],
            [
                
                'name' => 'โต๊ะ2',
                'branch_id' => 1,         
            ],
            [
                
                'name' => 'โต๊ะ3',
                'branch_id' => 1,         
            ],
            [
                
                'name' => 'โต๊ะ4',
                'branch_id' => 2,         
            ],
            [
                
                'name' => 'โต๊ะ5',
                'branch_id' => 1,         
            ],
            [
                
                'name' => 'โต๊ะ6',
                'branch_id' => 1,         
            ],
            [
                
                'name' => 'โต๊ะ7',
                'branch_id' => 1,         
            ],
            [
                
                'name' => 'โต๊ะ8',
                'branch_id' => 1,         
            ],
            [
                
                'name' => 'โต๊ะ9',
                'branch_id' => 1,         
            ],
            [
                
                'name' => 'โต๊ะ10',
                'branch_id' => 1,         
            ],
        ]);
    }
}
